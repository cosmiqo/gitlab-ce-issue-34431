+++
title = "Street Lighting along Persiaran Multimedia, Cyberjaya"
subtitle = "Why we do what we do"
date = "2017-06-25"
splash = "../../images/splash/street-lighting.jpg"
+++

At the end of 2016, we were told that accidents frequently occur at night along
Persiaran Multimedia, Cyberjaya. This could be due to many factors one of which
is lighting conditions, a factor that is designed and controllable (you can see
where this article is going). We took Litesense® out for a spin. 

Accidents are undesirable and with that in mind, we performed roadway lighting
measurement for each lane along Persiaran Multimedia. In this case, there are
two lanes on either side. By convention, Lane 1 is closer to the divider (the
fast lane) and Lane 2 further away from the divider (the slow lane).

# Lighting Audit Report

These reports show the number of dark segments and its respective locations.
Please drive safely along Persiaran Multimedia and ask your local council to
improve lighting conditions. It is your right.

<figure>
<figcaption>
  1. From Persiaran APEC to Persiaran Multimedia Lane 1
</figcaption><img src=
"../../images/litesense/2016-12-28-cyberjaya/apec-multimedia-lane-1.png">
</figure>

<figure>
<figcaption>
  2. From Persiaran APEC to Persiaran Multimedia Lane 2
</figcaption><img src=
"../../images/litesense/2016-12-28-cyberjaya/apec-multimedia-lane-2.png">
</figure>

<figure>
<figcaption>
  3. From Persiaran Multimedia to Persiaran APEC Lane 1
</figcaption><img src=
"../../images/litesense/2016-12-28-cyberjaya/multimedia-apec-lane-1.png">
</figure>

<figure>
<figcaption>
  4. From Persiaran Multimedia to Persiaran APEC Lane 2
</figcaption><img src=
"../../images/litesense/2016-12-28-cyberjaya/multimedia-apec-lane-2.png">
</figure>

  <div class="source">
    (Cosmiqo Litesense®, 28 December 2016)
  </div>
  <div class="source">
    Photo by Inés Outumuro on <a href=
    "https://unsplash.com/photos/KD0nM18EcfM" target=
    "_blank">Unsplash</a>
  </div>
